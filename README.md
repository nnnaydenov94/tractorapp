## Tractor Backend App

This is the backend for the task.
You can run it using mvn clean install.
The application runs on port 8085.

Technological stack:
	1. Spring Boot
	2. Basic Auth
	3. Hibernate
	4. H2 for the persistence layer

Note: 
User: user ; Password: password. 
The credentials are hardcoded for the sake of self-containment of this task.

Some sample data is inserted in tractor and parcel using data.sql