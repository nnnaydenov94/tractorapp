package com.intracol.TractorApp.service.impl;

import com.intracol.TractorApp.model.Parcel;
import com.intracol.TractorApp.repo.ParcelRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class ParcelServiceImplTest {

    @Mock
    private ParcelRepository mockParcelRepository;

    @InjectMocks
    private ParcelServiceImpl parcelServiceImplUnderTest;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void testGetAllParcels() {
        List<Parcel> list = new ArrayList<>();
        list.add(new Parcel(1L, "test1", "crop1", 1.0));
        list.add(new Parcel(2L, "test2", "crop2", 1.0));
        list.add(new Parcel(3L, "test3", "crop3", 1.0));
        List<Parcel> result = null;

        when(mockParcelRepository.findAll()).thenReturn(list);
        result = parcelServiceImplUnderTest.getAllParcels();

        assertEquals(3, result.size());
        verify(mockParcelRepository, times(1)).findAll();
    }

    @Test
    public void testSaveParcel() {
        // Setup
        final Parcel parcel = new Parcel(null, "test1", "crop1", 1.0);
        parcelServiceImplUnderTest.saveParcel(parcel);

        // Verify the results
        verify(mockParcelRepository, times(1)).save(parcel);
    }
}
