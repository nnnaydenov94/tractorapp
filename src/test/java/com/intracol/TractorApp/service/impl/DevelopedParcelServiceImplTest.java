package com.intracol.TractorApp.service.impl;

import com.intracol.TractorApp.exception.DevelopedAreaTooLargeException;
import com.intracol.TractorApp.model.DevelopedParcel;
import com.intracol.TractorApp.model.Parcel;
import com.intracol.TractorApp.model.Tractor;
import com.intracol.TractorApp.model.dto.ParcelAndTractorContainerDTO;
import com.intracol.TractorApp.repo.DevelopedParcelRepository;
import com.intracol.TractorApp.repo.ParcelRepository;
import com.intracol.TractorApp.repo.TractorRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class DevelopedParcelServiceImplTest {

    @Mock
    private DevelopedParcelRepository mockDevelopedParcelRepository;
    @Mock
    private TractorRepository mockTractorRepository;
    @Mock
    private ParcelRepository mockParcelRepository;

    @InjectMocks
    private DevelopedParcelServiceImpl developedParcelServiceImplUnderTest;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void testGetAllDevelopedParcels() {
        List<DevelopedParcel> listDevelopedParcels = new ArrayList<>();
        List<DevelopedParcel> result = new ArrayList<>();
        Tractor tractor = new Tractor(1L, "name");
        Parcel parcel = new Parcel(1L, "name", "crop", 10.0);
        Tractor tractor2 = new Tractor(2L, "name2");
        Parcel parcel2 = new Parcel(2L, "name2", "crop2", 20.0);


        listDevelopedParcels.add(new DevelopedParcel(1L, "1/1/1990", 1.0,
                mockTractorRepository.getById(tractor.getId()), mockParcelRepository.getById(parcel.getId())));
        listDevelopedParcels.add(new DevelopedParcel(2L, "2/2/1992", 2.0,
                mockTractorRepository.getById(tractor2.getId()), mockParcelRepository.getById(parcel2.getId())));
        when(mockTractorRepository.getById(anyLong())).thenReturn(tractor);
        when(mockTractorRepository.getById(anyLong())).thenReturn(tractor2);
        when(mockParcelRepository.getById(anyLong())).thenReturn(parcel);
        when(mockParcelRepository.getById(anyLong())).thenReturn(parcel2);
        when(mockDevelopedParcelRepository.findAll()).thenReturn(listDevelopedParcels);
        result = developedParcelServiceImplUnderTest.getAllDevelopedParcels();
        assertEquals(2, result.size());
        assertEquals(mockDevelopedParcelRepository.findAll(), result);
        verify(mockDevelopedParcelRepository, times(2)).findAll();
    }

    @Test
    public void testSaveDevelopedParcel() throws Exception {
        ParcelAndTractorContainerDTO parcelAndTractorContainerDTO = new ParcelAndTractorContainerDTO(1L, 1L, 1.0, new Date());
        Parcel parcel = new Parcel(1L, "name", "crop", 1.0);
        Tractor tractor = new Tractor(1L, "name");

        when(mockParcelRepository.getById(parcelAndTractorContainerDTO.getParcelId())).thenReturn(parcel);
        when(mockTractorRepository.getById(parcelAndTractorContainerDTO.getTractorId())).thenReturn(tractor);
        when(mockDevelopedParcelRepository.save(any(DevelopedParcel.class))).thenReturn(null);

        developedParcelServiceImplUnderTest.saveDevelopedParcel(parcelAndTractorContainerDTO);

        verify(mockParcelRepository, times(1)).getById(parcelAndTractorContainerDTO.getParcelId());
        verify(mockDevelopedParcelRepository, times(1)).save(any(DevelopedParcel.class));
    }

    @Test(expected = DevelopedAreaTooLargeException.class)
    public void testSaveDevelopedParcel_ThrowsDevelopedAreaTooLargeException() throws Exception {
        ParcelAndTractorContainerDTO parcelAndTractorContainerDTO = new ParcelAndTractorContainerDTO(1L, 1L, 10.0, new Date());
        Parcel parcel = new Parcel(1L, "name", "crop", 1.0);
        Tractor tractor = new Tractor(1L, "name");
        when(mockParcelRepository.getById(parcelAndTractorContainerDTO.getParcelId())).thenReturn(parcel);
        when(mockTractorRepository.getById(parcelAndTractorContainerDTO.getTractorId())).thenReturn(tractor);
        when(mockDevelopedParcelRepository.save(any(DevelopedParcel.class))).thenReturn(null);

        developedParcelServiceImplUnderTest.saveDevelopedParcel(parcelAndTractorContainerDTO);
    }
}
