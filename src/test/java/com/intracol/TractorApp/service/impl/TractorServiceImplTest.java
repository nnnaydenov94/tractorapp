package com.intracol.TractorApp.service.impl;

import com.intracol.TractorApp.model.Tractor;
import com.intracol.TractorApp.repo.TractorRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class TractorServiceImplTest {

    @Mock
    private TractorRepository mockTractorRepository;
    @Mock
    private ModelMapper mockModelMapper;

    @InjectMocks
    private TractorServiceImpl tractorServiceImplUnderTest;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void testGetAllTractors() {
       List<Tractor> list = new ArrayList<>();
       list.add(new Tractor(1L, "name"));
       list.add(new Tractor(2L, "name2"));
       List<Tractor> result = null;

       when(mockTractorRepository.findAll()).thenReturn(list);
       result = tractorServiceImplUnderTest.getAllTractors();
       assertEquals(2, result.size());
       verify(mockTractorRepository, times(1)).findAll();
    }

    @Test
    public void testSaveTractor() {
        final Tractor tractor = new Tractor(null, "name");
        tractorServiceImplUnderTest.saveTractor(tractor);
        verify(mockTractorRepository, times(1)).save(tractor);
    }
}
