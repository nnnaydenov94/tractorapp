package com.intracol.TractorApp.exception;

/**
 * This exception is thrown if user inputs developed area larger than crop's area.
 */
public class DevelopedAreaTooLargeException extends Exception {

    /**
     *
     * @param errorMessage
     */
    public DevelopedAreaTooLargeException(String errorMessage){
        super(errorMessage);
    }
}
