package com.intracol.TractorApp.service;

import com.intracol.TractorApp.model.Parcel;

import java.util.List;

public interface ParcelService {
    /**
     * @return all parcels from the database
     */
    List<Parcel> getAllParcels();

    /**
     * @param parcel
     * @return parcel saved to database
     */
    void saveParcel(Parcel parcel);
}
