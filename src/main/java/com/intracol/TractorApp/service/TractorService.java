package com.intracol.TractorApp.service;

import com.intracol.TractorApp.model.Tractor;
import com.intracol.TractorApp.model.dto.TractorDTO;

import java.util.List;


public interface TractorService {

    /**
     *
     * @return all tractors from database
     */
    List<Tractor> getAllTractors();

    /**
     *
     * @param tractor
     * @return saved tractor
     */
    void saveTractor(Tractor tractor);

}
