package com.intracol.TractorApp.service;

import com.intracol.TractorApp.exception.DevelopedAreaTooLargeException;
import com.intracol.TractorApp.model.DevelopedParcel;
import com.intracol.TractorApp.model.dto.DevelopedParcelDTO;
import com.intracol.TractorApp.model.dto.ParcelAndTractorContainerDTO;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.List;

public interface DevelopedParcelService {
    /**
     * @return a list of all developed parcel entries
     */
    List<DevelopedParcel> getAllDevelopedParcels();

    /**
     *
     * @param parcelAndTractorContainerDTO
     * @return saved object from database
     * @throws DevelopedAreaTooLargeException
     */

    DevelopedParcel saveDevelopedParcel(ParcelAndTractorContainerDTO parcelAndTractorContainerDTO) throws DevelopedAreaTooLargeException;
}
