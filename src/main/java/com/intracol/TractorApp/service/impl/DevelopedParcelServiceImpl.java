package com.intracol.TractorApp.service.impl;

import com.intracol.TractorApp.exception.DevelopedAreaTooLargeException;
import com.intracol.TractorApp.model.DevelopedParcel;
import com.intracol.TractorApp.model.Parcel;
import com.intracol.TractorApp.model.Tractor;
import com.intracol.TractorApp.model.dto.DevelopedParcelDTO;
import com.intracol.TractorApp.model.dto.ParcelAndTractorContainerDTO;
import com.intracol.TractorApp.repo.DevelopedParcelRepository;
import com.intracol.TractorApp.repo.ParcelRepository;
import com.intracol.TractorApp.repo.TractorRepository;
import com.intracol.TractorApp.service.DevelopedParcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DevelopedParcelServiceImpl implements DevelopedParcelService {

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");
    @Autowired
    private DevelopedParcelRepository developedParcelRepository;
    @Autowired
    private TractorRepository tractorRepository;
    @Autowired
    private ParcelRepository parcelRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DevelopedParcel> getAllDevelopedParcels() {
        return developedParcelRepository.findAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DevelopedParcel saveDevelopedParcel(ParcelAndTractorContainerDTO parcelAndTractorContainerDTO) throws DevelopedAreaTooLargeException {
        DevelopedParcel newDevelopedParcel = new DevelopedParcel();
        Parcel parcel = parcelRepository.getById(parcelAndTractorContainerDTO.getParcelId());
        Tractor tractor = tractorRepository.getById(parcelAndTractorContainerDTO.getTractorId());
        String date = dateFormat.format(parcelAndTractorContainerDTO.getDate());
        newDevelopedParcel.setParcel(parcel);
        newDevelopedParcel.setTractor(tractor);
        newDevelopedParcel.setDate(date);
        if (parcelAndTractorContainerDTO.getDevelopedArea() > parcel.getArea()) {
            throw new DevelopedAreaTooLargeException("Developed area cannot be larger than the parcel area." +
                    " Parcel area is: " + parcel.getArea());
        } else {
            newDevelopedParcel.setDevelopedArea(parcelAndTractorContainerDTO.getDevelopedArea());
        }

        return developedParcelRepository.save(newDevelopedParcel);
    }




}
