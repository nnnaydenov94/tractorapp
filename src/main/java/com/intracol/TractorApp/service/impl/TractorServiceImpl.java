package com.intracol.TractorApp.service.impl;

import com.intracol.TractorApp.model.Tractor;
import com.intracol.TractorApp.model.dto.TractorDTO;
import com.intracol.TractorApp.repo.TractorRepository;
import com.intracol.TractorApp.service.TractorService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TractorServiceImpl implements TractorService {
    @Autowired
    private TractorRepository tractorRepository;
    @Autowired
    private ModelMapper modelMapper;

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public List<Tractor> getAllTractors() {
        return tractorRepository.findAll();
    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public void saveTractor(Tractor tractor) {
        tractorRepository.save(tractor);
    }


}
