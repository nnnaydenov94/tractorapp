package com.intracol.TractorApp.service.impl;

import com.intracol.TractorApp.model.Parcel;
import com.intracol.TractorApp.model.dto.ParcelDTO;
import com.intracol.TractorApp.repo.ParcelRepository;
import com.intracol.TractorApp.service.ParcelService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ParcelServiceImpl implements ParcelService {
    @Autowired
    private ParcelRepository parcelRepository;

    @Autowired
    private ModelMapper modelMapper;

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public List<Parcel> getAllParcels() {
        return parcelRepository.findAll();
    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public void saveParcel(Parcel parcel) {
        parcelRepository.save(parcel);
    }
}
