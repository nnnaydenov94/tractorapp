package com.intracol.TractorApp.model;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
public class DevelopedParcel implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String date;

    @NotNull
    private Double developedArea;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "tractor_id", referencedColumnName = "id")
    private Tractor tractor;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "parcel_id", referencedColumnName = "id")
    private Parcel parcel;
}
