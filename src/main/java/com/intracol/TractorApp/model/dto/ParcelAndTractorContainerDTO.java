package com.intracol.TractorApp.model.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ParcelAndTractorContainerDTO {

    @NotNull
    private Long parcelId;
    @NotNull
    private Long tractorId;
    @NotNull
    private double developedArea;
    @NotNull
    private Date date;

}
