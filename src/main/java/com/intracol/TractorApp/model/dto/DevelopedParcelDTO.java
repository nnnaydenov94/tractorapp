package com.intracol.TractorApp.model.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DevelopedParcelDTO {
    private Long id;
    private String tractorName;
    private String cropName;
    private String parcelName;
    private String date;
    private Double developedArea;
}
