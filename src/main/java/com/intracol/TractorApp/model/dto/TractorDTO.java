package com.intracol.TractorApp.model.dto;

import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TractorDTO {
    private Long id;
    @NotNull
    private String name;
}
