package com.intracol.TractorApp.repo;

import com.intracol.TractorApp.model.DevelopedParcel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DevelopedParcelRepository extends CrudRepository<DevelopedParcel, Long> {

    /**
     *
     * @return all developed parcels in the database
     */
    List<DevelopedParcel> findAll();
}
