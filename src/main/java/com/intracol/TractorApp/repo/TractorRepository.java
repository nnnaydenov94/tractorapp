package com.intracol.TractorApp.repo;

import com.intracol.TractorApp.model.Tractor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TractorRepository extends CrudRepository<Tractor, Long> {

    /**
     *
     * @return all tractors in the database
     */
    List<Tractor> findAll();

    /**
     *
     * @param id
     * @return tractor with id
     */
    Tractor getById(Long id);
}
