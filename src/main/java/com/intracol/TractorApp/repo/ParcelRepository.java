package com.intracol.TractorApp.repo;

import com.intracol.TractorApp.model.Parcel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParcelRepository extends CrudRepository<Parcel, Long> {

    /**
     *
     * @return all parcels in the database
     */
    List<Parcel> findAll();

    /**
     *
     * @param id
     * @return parcel with id
     */
    Parcel getById(Long id);
}
