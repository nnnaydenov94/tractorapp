package com.intracol.TractorApp.controller;

import com.intracol.TractorApp.model.Tractor;
import com.intracol.TractorApp.model.dto.TractorDTO;
import com.intracol.TractorApp.service.impl.TractorServiceImpl;
import lombok.extern.java.Log;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller to get/create tractors
 */

@Log
@RestController
@CrossOrigin
public class TractorController {

    @Autowired
    TractorServiceImpl tractorService;
    @Autowired
    ModelMapper modelMapper;


    @GetMapping(value = "/tractors/get")
    public List<TractorDTO> getAllTractors() {
        return tractorService.getAllTractors()
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @PostMapping(value = "/tractors/create")
    public ResponseEntity createTractor(@RequestBody TractorDTO tractorDTO) {
        try {
            tractorService.saveTractor(convertToEntity(tractorDTO));
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            log.warning(e.getMessage());
            return new ResponseEntity<>("One or more of the properties is not valid.", HttpStatus.BAD_REQUEST);
        }
    }

    //Helper methods
    private TractorDTO convertToDTO(Tractor tractor) {
        return modelMapper.map(tractor, TractorDTO.class);
    }

    private Tractor convertToEntity(TractorDTO tractorDTO) {
        return modelMapper.map(tractorDTO, Tractor.class);
    }
}
