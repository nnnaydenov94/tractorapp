package com.intracol.TractorApp.controller;

import com.intracol.TractorApp.exception.DevelopedAreaTooLargeException;
import com.intracol.TractorApp.model.DevelopedParcel;
import com.intracol.TractorApp.model.dto.DevelopedParcelDTO;
import com.intracol.TractorApp.model.dto.ParcelAndTractorContainerDTO;
import com.intracol.TractorApp.service.impl.DevelopedParcelServiceImpl;
import com.intracol.TractorApp.service.impl.ParcelServiceImpl;
import com.intracol.TractorApp.service.impl.TractorServiceImpl;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller to get/create developed parcels
 */

@Log
@RestController
@CrossOrigin
public class DevelopedParcelController {
    @Autowired
    private DevelopedParcelServiceImpl developedParcelSevice;
    @Autowired
    private TractorServiceImpl tractorService;
    @Autowired
    private ParcelServiceImpl parcelService;


    @GetMapping(value = "/developedParcels/get")
    List<DevelopedParcelDTO> getDevelopedParcels() {
        return developedParcelSevice.getAllDevelopedParcels()
                .stream().map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @PostMapping(value = "/developedParcels/create")
    ResponseEntity createDevelopedParcel(@RequestBody ParcelAndTractorContainerDTO parcelAndTractorContainerDTO) {
        try {
            developedParcelSevice.saveDevelopedParcel(parcelAndTractorContainerDTO);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (DevelopedAreaTooLargeException e) {
            log.warning(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e){
            return new ResponseEntity<>("One or more of the properties is not valid.", HttpStatus.BAD_REQUEST);
        }
    }

    //Helper methods

    /**
     * @param developedParcel
     * @return DevelopedParcel converted to DevelopedParcelDTO
     */
    private DevelopedParcelDTO convertToDto(DevelopedParcel developedParcel) {
        DevelopedParcelDTO developedParcelDTO = new DevelopedParcelDTO();
        developedParcelDTO.setId(developedParcel.getId());
        developedParcelDTO.setCropName(developedParcel.getParcel().getCrop());
        developedParcelDTO.setDevelopedArea(developedParcel.getDevelopedArea());
        developedParcelDTO.setDate(developedParcel.getDate());
        developedParcelDTO.setParcelName(developedParcel.getParcel().getName());
        developedParcelDTO.setTractorName(developedParcel.getTractor().getName());
        return developedParcelDTO;
    }
}
