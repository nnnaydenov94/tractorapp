package com.intracol.TractorApp.controller;

import com.intracol.TractorApp.model.User;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * This endpoint will return a login successful upon correct credentials input
 */

@RestController
@CrossOrigin
public class AuthenticationController {

    @RequestMapping(value = "/authenticate", method = RequestMethod.GET)
    public User authenticate() {
        return new User("Login successful.");
    }


}
