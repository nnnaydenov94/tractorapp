package com.intracol.TractorApp.controller;

import com.intracol.TractorApp.model.Parcel;
import com.intracol.TractorApp.model.dto.ParcelDTO;
import com.intracol.TractorApp.service.impl.ParcelServiceImpl;
import lombok.extern.java.Log;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller to get/create parcels
 */

@Log
@RestController
@CrossOrigin
public class ParcelController {

    @Autowired
    ModelMapper modelMapper;
    @Autowired
    private ParcelServiceImpl parcelService;

    @GetMapping(value = "/parcels/get")
    List<ParcelDTO> getAllParcels() {
        return parcelService.getAllParcels()
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @PostMapping(value = "/parcels/create")
    ResponseEntity createParcel(@RequestBody ParcelDTO parcelDTO) {
        try {
            parcelService.saveParcel(convertToEntity(parcelDTO));
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            log.warning(e.getMessage());
            return new ResponseEntity<>("One or more of the properties is not valid.", HttpStatus.BAD_REQUEST);
        }
    }

    //Helper methods
    private ParcelDTO convertToDTO(Parcel parcel) {
        return modelMapper.map(parcel, ParcelDTO.class);
    }

    private Parcel convertToEntity(ParcelDTO parcelDTO) {
        return modelMapper.map(parcelDTO, Parcel.class);
    }

}
